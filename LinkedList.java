public class LinkedList
{
  Node first;

  public LinkedList()
  {
    first = null;
  }

  public boolean isEmpty()
  {
    return (first == null);
  }

  public void addFirst(String data)
  {
    Node node = new Node(data);
    node.next = first;
    first = node;
  }

  public void addLast(String data)
  {
    Node node, temp;
    node = new Node(data);
    node.next = null;

    if(isEmpty()){
      first = node;
      first.next = null;
    }
    else{
      temp = first;
      while(temp.next != null){
        temp = temp.next;
      }
      temp.next = node;
    }
  }

  public Node deleteFirst()
  {
    if(!isEmpty()){
      Node temp = first;
      first = first.next;
      return temp;
    }
    else{
      return null;
    }
  }

  public Node deleteLast()
  {
    if(!isEmpty()){
      Node temp, current;
      current = first;
      while(current.next.next != null){
        current = current.next;
      }
      temp = current.next;
      current.next = null;
      return temp;
    }
    else{
      Node temp = first;
      first = null;
      return temp;
    }
  }

  public void searchList(String cari)
  {
    Node temp = first;
    int counter = 1;
    boolean ketemu = false;
    
    while(temp != null){
      if(temp.data.toLowerCase().contains(cari.toLowerCase())){
        temp.tampil(counter);
        ketemu = true;
        counter++;
      }
      else if(!ketemu && temp.next == null){
        System.out.println("Data tidak ditemukan.");
      }
      temp = temp.next;
    }
  }

  public void sortList()
  {
    Node current = first;
    //Node current2 = first;
    Node ujung = null;
    boolean swapped;
    
    do{
      swapped = false;
      current = first;
      while(current.next != ujung){
        if(current.data.compareTo(current.next.data) > 0){
          String temp = current.data;
          current.data = current.next.data;
          current.next.data = temp;
          swapped = true;
        }
        current = current.next;
      }
      ujung = current;
    }while(swapped);
    
  }

  public void printList()
  {
    Node current = first;
    int counter = 1;

    if(current == null){
      System.out.println("List Kosong :)");
    }
    else{
      while (current != null){
        current.tampil(counter);
        current = current.next;
        counter++;
      }
      //System.out.println();
    }
  }
}
