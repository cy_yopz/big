public class QuickSort
{
  int[] arr;

  public QuickSort(int[] arr){
    this.arr = arr;
    quicksort(0, arr.length-1);
  }

  public void swap (int from, int to)
  {
    int temp = arr[from];
    arr[from] = arr[to];
    arr[to] = temp;
  }

  public void quicksort(int from, int to)
  {
    if (from >= to) return;
    int pivot = arr[to];
    int counter = from;
    
    for(int i=counter; i<to; i++){
      if (arr[i] < pivot){
        swap(i, counter);
        counter++;
      }
    }
    swap(counter, to);
    quicksort(from, counter-1);
    quicksort(counter+1, to);
  }

  public int[] getArray()
  {
    return arr;
  }
}
