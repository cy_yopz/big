class BinarySearchTree
{
  //BST Node
  class Node
  {
    String key;
    Node left, right;

    public Node(String item)
    {
      key = item;
      left = right = null;
    }
  }
  //BST Node

  Node root;

  public BinarySearchTree()
  {
    root = null;
  }

  public void insert(String key)
  {
    root = insert(root, key);
  }

  private Node insert(Node root, String key)
  {
    if(root == null){
      root = new Node(key);
      return root;
    }
    if(key.compareTo(root.key) < 0){
      root.left = insert(root.left, key);
    }
    else if(key.compareTo(root.key) > 0){
      root.right = insert(root.right, key);
    }
    return root;
  }

  public void printInOrder()
  {
    printInOrder(root);
  }

  private void printInOrder(Node root)
  {
    if(root != null){
      printInOrder(root.left);
      System.out.println(root.key);
      printInOrder(root.right);
    }
  }
}
