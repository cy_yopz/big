public class LinearSearch
{
  int[] arr;

  public LinearSearch(int[] arr)
  {
    this.arr = arr;
  }

  public int linearSearch(int[] arr, int x)
  {
    int n = arr.length;
    int i;

    for(i=0; i<arr.length; i++){
      if(arr[i] == x) return i;
    }
    return -1;
  }
}
